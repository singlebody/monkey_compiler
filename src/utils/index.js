export function isNull(obj) {
    return obj === null || obj === undefined
}

export function isNotNull(obj) {
    return !isNull(obj)
}

export function isEmpty(obj) {
    return isNull(obj) || obj === ''
}

export function isNotEmpty(obj) {
    return !isEmpty(obj)
}