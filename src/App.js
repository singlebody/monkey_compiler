import { Row, Col } from 'antd'
// import CompilerIdeBasic from './basic/MonkeyCompilerIDE'
import CompilerIdeParse from './components/MonkeyComplierIDE'

const App = () => {
  return <Row>
	<Col span={24}>
		<CompilerIdeParse />
	</Col>
  	{/* <Col span={12}>
	  <CompilerIdeBasic />
	</Col> */}
  </Row>
}

export default App
