export class TokenType {
    static ILLEGAL = -2
    static EOF = -1
    static LET = 0 // let
    static IDENTIFIER = 1 // 变量
    static ASSIGN_SIGN = 2 // =
    static PLUS_SIGN = 3 // +
    static INTEGER = 4 // 整数
    static SEMICOLON = 5 // ;
    static IF = 6 // if
    static ELSE = 7 // else
    static MINUS_SIGN = 8 // -
    static BANG_SIGN = 9 // !（非）
    static ASTERISK = 10 // *
    static SLASH = 11 // /
    static LT = 12 // <
    static GT = 13 // >
    static COMMA = 14 // ,
    static FUNCTION = 15 // function
    static TRUE = 16 // true
    static FALSE = 17 // false
    static RETURN = 18 // return
    static LEFT_BRACE = 19 // {
    static RIGHT_BRACE = 20 // }
    static EQ = 21 // ==
    static NOT_EQ = 22 // !=
    static LEFT_PARENT = 23 // (
    static RIGHT_PARENT = 24 // )
    static STRING = 25 // 字符串
    static LEFT_BRACKET = 26 // [
    static RIGHT_BRACKET = 27 // ]

    static COLON = 30 // :

    static getLiteralByTokenType(type) {
        switch (type) {
		    case TokenType.EOF:
                return "end of file"
		    case TokenType.LET:
                return "let"
		    case TokenType.IDENTIFIER:
                return "identifier"
		    case TokenType.ASSIGN_SIGN:
                return "assign sign"
		    case TokenType.PLUS_SIGN:
                return "plus sign"
		    case TokenType.INTEGER:
                return "integer"
		    case TokenType.SEMICOLON:
                return "semicolon"
		    case TokenType.IF:
                return "if"
		    case TokenType.ELSE:
                return "else"
		    case TokenType.MINUS_SIGN:
                return "minus sign"
		    case TokenType.BANG_SIGN:
                return "!"
		    case TokenType.ASTERISK:
                return "*"
		    case TokenType.SLASH:
                return "slash"
		    case TokenType.LT:
                return "<"
		    case TokenType.GT:
                return ">"
		    case TokenType.COMMA:
                return ","
		    case TokenType.FUNCTION:
                return "fun"
		    case TokenType.TRUE:
                return "true"
		    case TokenType.FALSE:
                return "fasle"
		    case TokenType.RETURN:
                return "return"
		    case TokenType.LEFT_BRACE:
                return "{"
		    case TokenType.RIGHT_BRACE:
                return "}"
		    case TokenType.EQ:
                return "=="
		    case TokenType.NOT_EQ:
                return "!="
		    case TokenType.LEFT_PARENT:
                return "("
		    case TokenType.RIGHT_PARENT:
                return ")"
            case TokenType.STRING:
                return ""
            case TokenType.LEFT_BRACKET:
                return "["
            case TokenType.RIGHT_BRACKET:
                return "]"
            case TokenType.COLON:
                return ":"
            default:
                return "unknow token"
		}
    }
}

export class TokenPriority {
    static LOWEST = 0
    static EQUALS = 1  // ==
    static LESSGREATER = 2 // < or >
    static SUM = 3 // +
    static PRODUCT = 4 // *
    static PREFIX = 5 //-X or !X
    static CALL = 6  // function(X)
    static INDEX = 7 // 数组取值具备最高优先级
}

export class ObjectTypes {
    static INTEGER_OBJ = "INTEGER"
    static BOOLEAN_OBJ = "BOOLEAN"
    static NULL_OBJ = "NULL"
    static ERROR_OBJ = "Error"
    static RETURN_VALUE_OBJECT = "Return"
    static FUNCTION_LITERAL = "FunctionLiteral"
    static FUNCTION_CALL = "FunctionCall"
    static STRING_OBJ = "STRING"
    static ARRAY_OBJ = "ARRAY"
    static HASH_OBJ = "HASH"
}