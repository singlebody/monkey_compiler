import { Expression } from "./expression"

export class ArrayLiteral extends Expression {

    _elements = []

    constructor(token, elements) {
        super(token)
        this.elements = elements
        this.literal = `array are: [(${this.elements.map(it => it.literal).reduce((p1, p2) => p1 + ", " + p2, "").substring(1)})]`
        this.type = "ArrayLiteral"
    }

    get elements() { return this._elements }
    set elements(elements) { this._elements = elements }

}