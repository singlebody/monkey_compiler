import { Expression } from "./expression";

export class BooleanExpression extends Expression {
    _value = ""
    constructor(token, value) {
        super(token)
        this.value = value
        this.literal = `Boolean token with value of ${this.value}`
        this.type = "Boolean"
    }

    get value() { return this._value }
    set value(value) { this._value = value }
}