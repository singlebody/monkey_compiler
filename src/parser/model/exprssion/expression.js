import { Node } from '../'
export class Expression extends Node {

    _token = null

    constructor(token) {
        super()
        this.token = token
        this.literal = token?.literal
        this.type = "Expression"
    }

    get token() { return this._token }
    set token(token) { this._token = token }

    expressionNode() {
        return this
    }
}