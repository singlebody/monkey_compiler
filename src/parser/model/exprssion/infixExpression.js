import { Expression } from "./expression"

export class InfixExpression extends Expression {
    _left = null
    _operator = null
    _right = null

    constructor(token, letf, operator, right) {
        super(token)
        this.left = letf
        this.operator = operator
        this.right = right
        this.literal = `(${this?.left?.literal} ${this.operator} ${this?.right?.literal})`
        this.type = "InfixExpression"
    }

    get left() { return this._left }
    set left(left) { this._left = left }

    get operator() { return this._operator }
    set operator(operator) { this._operator = operator }

    get right() { return this._right }
    set right(right) { this._right = right }
}