import { Expression } from "./expression"

export class CallExpression extends Expression {
    _fun = null
    _args = []
    constructor(token, fun, args) {
        super(token)
        this.fun = fun
        this.args = args
        this.literal = `It's a function call: ${this.fun.literal}
            It's input parameters are: (${this.args.map(it => it.literal).reduce((p1, p2) => p1 + ", " + p2, "").substring(1)})
        `
        this.type = "CallExpression"
    }

    get fun() {return this._fun }
    set fun(fun) {this._fun = fun }

    get args() {return this._args }
    set args(args) { this._args = args }
}