import { Expression } from "./expression";

export class IfExpression extends Expression{
    /**
     * IF 语句的条件
     */
    _condition = null
    /**
     * IF语句块儿
     */
    _consequence = null
    /**
     * ELSE语句块儿
     */
    _alternative = null

    constructor(token, condition, consequence, alternative) {
        super(token)
        this.condition = condition
        this.consequence = consequence
        this.alternative = alternative
        this.literal = `
            if expression with condition: ${this.condition.literal}
            statements in if block are: ${this.consequence.literal}
            ${this.alternative ? "statements in else block are: " + this.alternative.literal : "" }
        `
        this.type = "IfExpression"
    }

    set condition(t) {this._condition = t }
    get condition() { return this._condition }
    set consequence(t) {this._consequence = t }
    get consequence() { return this._consequence }
    set alternative(t) {this._alternative = t }
    get alternative() { return this._alternative }


}