import { Expression } from "./expression"
export class Identifier extends Expression {
    _value = ""
    constructor(token) {
        super(token)
        this.literal = token?.literal
        this.value = ""
        this.type = "Identifier"
    }

    get value() {
        return this._value
    }

    set value(value) {
        this._value = value
    }
}