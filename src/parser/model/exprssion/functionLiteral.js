import { Expression } from "./expression";

export class FunctionLiteral extends Expression {
    _parameters = []
    _body = null

    constructor(token, parameters, body) {
        super(token)
        this.parameters = parameters
        this.body = body
        this.literal = `It's a nameless function, input parameters are: (${this.parameters.map(it => it.literal).reduce((p1, p2) => p1 + ", " + p2, "").substring(1)})
            statements in function body are: {
                ${this.body.literal}
            }
        `
        this.type = "FunctionLiteral"
    }

    set parameters(parameters) { this._parameters = parameters }
    get parameters() { return this._parameters }

    set body(body) { this._body = body }
    get body() { return this._body }
}