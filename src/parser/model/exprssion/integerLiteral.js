import { Expression } from "./expression"

export class IntegerLiteral extends Expression {

    _value = null
    constructor(token, value) {
        super(token)
        this.value = value
        this.literal = `Integer value is: ${this.token.literal}`
        this.type = "Integer"
    }

    get value() {
        return this._value
    }

    set value(value) {
        this._value = value
    }
}