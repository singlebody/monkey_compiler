import { Expression } from "./expression";

export class PrefixExpression extends Expression {

    _operator = null
    _right = null

    constructor(token, operator, right) {
        super(token)
        this.operator = operator
        this.right = right
        this.literal = `(${this.operator + this.right.literal})`
        this.type = "PrefixExpression"
    }

    get operator() { return this._operator }
    set operator(operator) { this._operator = operator }

    get right() { return this._right }
    set right(right) { this._right = right }
}