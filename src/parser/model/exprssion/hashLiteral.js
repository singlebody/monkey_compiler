import { Expression } from "./expression"

export class HashLiteral extends Expression {
    _keys = []
    _values = []
    constructor(token, keys, values) {
        super(token)
        this.keys = keys
        this.values = values
        let str = "{"
        for(let i = 0; i < this.keys.length; i++) {
            str += `${this.keys[i].literal}: ${this.values[i].literal}`
        }
        this.literal = str + "}"
        this.type = "HashLiteral"
    }

    get keys() { return this._keys }
    set keys(keys) { this._keys = keys  }

    get values() { return this._values }
    set values(values) { this._values = values }
}