import { Expression } from "./expression"

export class StringLiteral extends Expression {

    _value = null
    constructor(token) {
        super(token)
        this.value = token.literal
        this.literal = `${this.token.literal}`
        this.type = "String"
    }

    get value() {
        return this._value
    }

    set value(value) {
        this._value = value
    }
}