import { Expression } from "./expression"

export class IndexExpression extends Expression {
    /**
     * left 就是[前面的表达式，它可以是变量名，数组，函数等
     */
    _left = null

    /**
     * index 可以是数字常量，变量，函数调用
     */
    _index = null
    constructor(token, left, index) {
        super(token)
        this.left = left
        this.index = index
        this.literal = `(${this.left.literal}[${this.index.literal}])`
        this.type = "IndexExpression"
    }

    set left(left) { this._left = left }
    get left() { return this._left}

    set index(index) { this._index = index }
    get index() { return this._index}
}