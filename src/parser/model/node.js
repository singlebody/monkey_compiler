export class Node {

    _literal = ""
    _type = null

    get literal() {
        return this._literal
    }

    set literal(value) {
        this._literal = value
    }

    get type() { return this._type }
    set type(type) {this._type = type }
}