export class Program {
    _statements = null
    _type = null

    constructor() {
        this._statements = []
        this.type = "program"
    }

    get statements() {
        return this._statements
    }

    set statements(stms) {
        this._statements = stms
    }

    get type() { return this._type }
    set type(type) { this._type = type }


    get literal() {
        if (this.statements.length > 0) {
            return this.statements[0].literal
        } else {
            return ""
        }
    }
}