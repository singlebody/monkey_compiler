import { ObjectTypes } from "../../constant"
import { BaseObject } from "./baseObject"


export class HashType extends BaseObject {
    _keys = []
    _values = []

    constructor(keys, values) {
        super()
        this.keys = keys
        this.values = values
    }

    type() { return ObjectTypes.HASH_OBJ }

    inspect() {
        let str = "{"
        for(let i = 0; i < this.keys.length; i++) {
            str += `${this.keys[i].inspect()}: ${this.values[i].inspect()}`
        }
        return str + "}"
    }

    get keys() { return this._keys }
    set keys(keys) { this._keys = keys  }

    get values() { return this._values }
    set values(values) { this._values = values }
}