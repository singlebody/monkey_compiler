import { BaseObject } from "./baseObject"
import { ObjectTypes } from '../../constant'

export class BooleanType extends BaseObject {
    _value = null
    constructor(value) {
        super()
        this.value = value
    }

    inspect() {
        return `boolean with value: ${this.value}`
    }

    type() {
        return ObjectTypes.BOOLEAN_OBJ
    }

    set value(value) {this._value = value}
    get value() {return this._value}
}