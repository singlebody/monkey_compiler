import { ObjectTypes } from "../../constant"
import { BaseObject } from "./baseObject"

export class ArrayType extends BaseObject {

    _elements = []

    constructor(elements) {
        super()
        this.elements = elements
    }

    type() {
        return ObjectTypes.ARRAY_OBJ
    }

    inspect() {
        return `[${this.elements.map(it => it.inspect()).reduce((s1, s2) => s1 + "," + s2).substring(1)}]`
    }

    get elements() { return this._elements }
    set elements(elements) { this._elements = elements }
}