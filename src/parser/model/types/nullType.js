import { BaseObject } from "./baseObject"
import { ObjectTypes } from '../../constant'

export class NullType extends BaseObject {

    type() {
        return ObjectTypes.NULL_OBJ
    }

    inspect() {
        return "null"
    }
}