import { ObjectTypes } from "../../constant"
import { BaseObject } from "./baseObject"

export class ReturnType extends BaseObject {
    _value = null
    constructor(value) {
        super()
        this.value = value
    }

    type() { return ObjectTypes.RETURN_VALUE_OBJECT }
    inspect() { return `return with: ${this.value.inspect()}`}

    get value() { return this._value }
    set value(value) { this._value = value }
}