import { BaseObject } from "./baseObject"
import { ObjectTypes } from '../../constant'

export class ErrorType extends BaseObject {
    _msg = null
    constructor(errMsg) {
        super()
        this.msg = errMsg
    }

    type() {
        return ObjectTypes.ERROR_OBJ
    }

    inspect() {
        return this.msg
    }

    set msg(msg) {this._msg = msg}
    get msg() { return this._msg }

}