import { BaseObject } from "./baseObject"
import { ObjectTypes } from '../../constant'

export class StringType extends BaseObject {
    _value = null
    constructor(value) {
        super()
        this.value = value
    }

    inspect() {
        return `content of string is: ${this.value}`
    }

    type() {
        return ObjectTypes.STRING_OBJ
    }

    set value(value) {this._value = value}
    get value() {return this._value}
}