import { BaseObject } from "./baseObject"
import { ObjectTypes } from '../../constant'

export class FunctionLiteralType extends BaseObject {
    _token = null
    _identifiers = null
    _blockStatement = null
    _enviroment = null

    constructor(token, identifiers, blockStatement, env) {
        super()
        this.token = token
        this.identifiers = identifiers
        this.blockStatement = blockStatement
        this.enviroment = env
    }

    type() {
        return ObjectTypes.FUNCTION_LITERAL
    }

    inspect() {
        return `fn(${this.identifiers.map(it => it.literal).reduce((p1, p2) => p1 + ", " + p2, "").substring(1)} {
            ${this.blockStatement.literal}
        }`
    }


    set token(token) { this._token = token}
    get token() { return this._token }
    set identifiers(identifiers) { this._identifiers = identifiers}
    get identifiers() { return this._identifiers }
    set blockStatement(blockStatement) { this._blockStatement = blockStatement}
    get blockStatement() { return this._blockStatement }
    get enviroment() { return this._enviroment }
    set enviroment(env) { this._enviroment = env }
}