import { BaseObject } from "./baseObject"
import { ObjectTypes } from '../../constant'

export class IntegerType extends BaseObject {
    _value = null
    constructor(value) {
        super()
        this.value = value
    }

    inspect() {
        return `integer with value: ${this.value}`
    }

    type() {
        return ObjectTypes.INTEGER_OBJ
    }

    set value(value) {this._value = value}
    get value() {return this._value}
}