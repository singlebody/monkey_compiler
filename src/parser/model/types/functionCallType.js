import { BaseObject } from "./baseObject"
import { ObjectTypes } from '../../constant'

export class FunctionCallType extends BaseObject {
    _args = null
    _functionLiteral = null

    constructor(args, functionLiteral) {
        super()
        this.args = args
        this.functionLiteral = functionLiteral
    }

    type() { return ObjectTypes.FUNCTION_CALL }

    inspect() {
        return `fn(${this.identifiers.map(it => it.inspect()).reduce((p1, p2) => p1 + ", " + p2, "").substring(1)} {
                    ${this.functionLiteral.inspect()}
        }`
    }

    get args() { return this._args}
    set args(obj) { this._args = obj }
    get functionLiteral() { return this._functionLiteral}
    set functionLiteral(obj) { this._functionLiteral = obj }
}