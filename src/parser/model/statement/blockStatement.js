import { Statement } from "./statement"

export class BlockStatement extends Statement {
    _statements = []
    constructor(token, statements) {
        super(token)
        this.statements = statements
        this.literal = this.statements.map(it => it.literal).reduce((s1, s2) => `${s1}\n${s2}`, "")
        this.type = "BlockStatement"
    }

    get statements() { return this._statements }
    set statements(statements) { this._statements = statements }
}