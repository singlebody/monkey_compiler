import { Statement } from './statement'

export class ReturnStatement extends Statement {
    _expression = null
    constructor(token, expression) {
        super(token)
        this.expression = expression
        // Describe Statement
        this.literal = `return with ${expression.literal}`
        this.type = "ReturnStatement"
    }

    get expression() {
        return this._expression
    }

    set expression(exp) {
        this._expression = exp
    }
}