import { Statement } from './statement'
export class LetStatement extends Statement {
    _name = null
    _value = null
    constructor(token, identifier, expression) {
        super(token)
        // IDENTIFIER Token (x)
        this.name = identifier
        // EXPRESSION(INTEGER) Token And SEMICOLON Token (1;)
        this.value = expression

        // Describe Statement
        this.literal = `This is a Let statement, left is an identifer:${this.name.literal} right size is value of ${this.value.literal}`
        this.type = "LetStatement"
    }

    get name() {
        return this._name
    }

    set name(identifer) {
        this._name = identifer
    }

    get value() {
        return this._value
    }

    set value(value) {
        this._value = value
    }
}