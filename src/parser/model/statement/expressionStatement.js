import { Statement } from './statement'
export class ExpressionStatement extends Statement {
    _expression = null
    constructor(token, expression) {
        super(token)
        this.expression = expression
        // Describe Statement
        this.literal = `expression: ${expression.literal}`
        this.type = "ExpressionStatement"
    }

    get expression() {
        return this._expression
    }

    set expression(exp) {
        this._expression = exp
    }
}
