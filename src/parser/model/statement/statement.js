// 语法解析树中的一个节点
import { Node } from '../'

export class Statement extends Node {

    _token = null

    constructor(token) {
        super()
        this.token = token
        this.type = "Statement"
    }

    statementNode() {
        return this
    }

}