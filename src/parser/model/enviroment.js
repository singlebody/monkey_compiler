import {isNotNull} from '../../utils'

export default class Enviroment {
    _map = null
    _outer = null
    constructor() {
        this.map = {}
        this.outer = null
    }

    get map() { return this._map }
    set map(map) { this._map = map }

    get outer() { return this._outer }
    set outer(outer) { this._outer = outer }

    get(name) {
        let obj = this.map[name]
        if (isNotNull(obj)) {
            return obj
        } else if (isNotNull(this.outer)) {
            obj = this.outer.get(name)
        }
        return obj
    }

    set(name, obj) {
        this.map[name] = obj
    }
}