export class Token {
    constructor(type, literal, lineNumber, begin, end) {
        this.type = type
        this.literal = literal
        this.lineNumber = lineNumber
        this.begin = begin
        this.end = end
    }

    get type() {
        return this._type
    }

    set type(type) {
        this._type = type
    }

    get literal() {
        return this._literal
    }

    set literal(literal) {
        this._literal = literal
    }

    get lineNumber() {
        return this._lineNumber
    }

    set lineNumber(lineNumber) {
        this._lineNumber = lineNumber
    }

    get begin() {
        return this._begin
    }

    set begin(begin) {
        this._begin = begin
    }

    get end() {
        return this._end
    }

    set end(end) {
        this._end = end
    }

}