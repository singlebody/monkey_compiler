import React , {Component} from 'react'
import { Button, Card, Typography } from 'antd'
import MonkeyLexer from './MonkeyLexer'
import MonkeyCompilerEditer from './MonkeyCompilerEditer'
import MonkeyCompilerParser from './MonkeyCompilerParser'
import 'antd/dist/antd.css'

const { Title } = Typography

class MonkeyCompilerIDE extends Component {
    constructor(props) {
        super(props)
        this.lexer = new MonkeyLexer("")
    }
    // change here
    onLexingClick () {
      this.lexer = new MonkeyLexer(this.inputInstance.getContent())
      this.parser = new MonkeyCompilerParser(this.lexer)
      this.parser.parseProgram()
      this.program = this.parser.program
      for (var i = 0; i < this.program.statements.length; i++) {
          console.log(this.program.statements[i].getLiteral())
      }
    }

    render () {
        // change here
        return (
          <Card title={<Title level={3}>Monkey Compiler Basic</Title>}
              actions={[<Button type='primary' danger size='large' onClick={this.onLexingClick.bind(this)}>Parse</Button>]}>
            <MonkeyCompilerEditer ref={(ref) => {this.inputInstance = ref}} keyWords={this.lexer.getKeyWords()} />
          </Card>
		);
    }
}

export default MonkeyCompilerIDE