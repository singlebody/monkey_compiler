import { Button, Card, Typography } from 'antd'
import Lexer from '../parser/monkeyLexer'
import { useMemo, useState } from 'react'
import MonkeyCompilerEditer from './MonkeyCompilerEditer'
import 'antd/dist/antd.css'
import Parser from '../parser/monkeyParser'
import Evaluator from '../parser/monkeyEvaluator'

const { Title } = Typography

function MonkeyComplierIDE() {
	const [source, setSource] = useState()
	const lexer = useMemo(() => new Lexer(source), [source])
	const evaluator = useMemo(() => new Evaluator(), [])
	// useEffect(() => {
	// 	if (lexer.isReady()) {
	// 		lexer.lexing()
	// 	}
	// }, [lexer])
	const onInput = e => setSource(e.target.innerText)

	const lexing = () => {
		const parser = new Parser(lexer)
		const programs = parser.parseProgram()
		// programs.statements.forEach(stms => {
		// 	console.log(stms.literal)
		// 	evaluator.eval(stms)
		// })
		evaluator.eval(programs)
	}
	return (
		<Card title={<Title level={3}>Monkey Compiler Copy</Title>}
			actions={[<Button type='primary' danger size='large' onClick={lexing}>Parse</Button>]}>
			<MonkeyCompilerEditer onInput={onInput} />
		</Card>
	)
}

export default MonkeyComplierIDE
