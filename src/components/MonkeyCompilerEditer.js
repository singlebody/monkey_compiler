import { useEffect } from "react"
import event from "../utils/event"

const MonkeyCompilerEditer = ({onInput}) => {
    useEffect(() => {
        event.addEventListener('keyword', (parmas) => {
            console.info(parmas)
        })
        return () => {
            event.removeEventListener('keyword')
        }
    }, [])
    let textAreaStyle = {
        height: 480,
        border: "1px solid black",
        textIndent: 8
    }
    return <div style={textAreaStyle} contentEditable onKeyUp={onInput}></div>
}

export default MonkeyCompilerEditer
